# Ansible-Vagrant-Docker

```plantuml
@startuml
package "Host" {
    [browser-app]
}

cloud "Vagrant-Ansible" {


  package "Server private_ip: 192.168.55.55" {
      [node-app]
  }


  database "PostgreSQL" {
      folder "database" {
          [users]
      }
  }

}

note right of (node-app)
 docker_container
 node_ip:192.168.55.55:80
end note

note right of (users)
 docker_container
 db_ip: localhost:5432
end note

note right of (browser-app)
 app_ip: localhost:8082
end note


[browser-app] --> [node-app]
[node-app] --> [users]
@enduml
```
