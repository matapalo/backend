const supertest = require("supertest");
const app = require("../app");

const api = supertest.agent(app);

const user = { username: "superuser", password: "testi" };

/* beforeEach(async () => {
  const res = await post("/register", user);

  expect(res => console.log(res));
  expect(res.status).toBe(201);
  expect(res.body.message).toContain("Account created");
}); */

describe("register a user", () => {
  it("should return statuscode 201 and a message: Account created", async () => {
    const res = await post("/register", user);
    expect(res.body).toHaveProperty("message", "Account created");
    //expect(res.statusCode).toEqual(201);
    //expect(res.body.message).toContain("Account created");
  });
});

test("server returning a message", async () => {
  const res = await api.get("/index");

  expect(res.statusCode).toEqual(200);
  expect(res.body.message).toContain("hello from server");
});

function post(url, body) {
  const httpRequest = api.post(url);
  httpRequest.send(body);
  httpRequest.set("Accept", "application/json");
  //httpRequest.set("Origin", "http://localhost:3000");
  return httpRequest;
}
