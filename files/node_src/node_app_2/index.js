const http = require("http");
const express = require("express");
const app = express();

const server = http.createServer(app);

app.get("/", (req, res) => {
  res.status(200).send({ message: "hello from server 2" });
});

server.listen(3000, () => {
  console.log("Server listening on localhost:3000...");
});
