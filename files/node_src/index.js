const app = require("./app"); // express-sovellus
const http = require("http");
const config = require("./utils/config"); // node-configuraatio
const logger = require("./utils/logger"); // custom-loggeri

const server = http.createServer(app);

server.listen(config.PORT, () =>
  logger.info(`Server running at localhost:${config.PORT}`)
);
