#! /usr/bin/python3
import psycopg2
from psycopg2 import Error
from psycopg2.extensions import ISOLATION_LEVEL_AUTOCOMMIT

con = None
try:
    con = psycopg2.connect(
        user="postgres", password="postgres", host="127.0.0.1", port="5432"
    )

    con.set_isolation_level(ISOLATION_LEVEL_AUTOCOMMIT)

    cursor = con.cursor()

    cursor.execute(
        """
            DROP DATABASE IF EXISTS inspire;
        """
    )

    cursor.execute(
        """
            CREATE DATABASE inspire;
        """
    )

    con.close()
    cursor.close()

    con = psycopg2.connect(
        user="postgres",
        password="postgres",
        host="127.0.0.1",
        port="5432",
        database="inspire",
    )

    cursor = con.cursor()

    cursor.execute(
        """
            DROP TABLE IF EXISTS testtable CASCADE;
            CREATE TABLE testtable(
                teksti TEXT NOT NULL  
            );
        """
    )

    cursor.execute(
        """
            DROP TABLE IF EXISTS users CASCADE;
        """
    )

    cursor.execute(
        """CREATE TABLE users(
                id UUID NOT NULL,
                username VARCHAR(30) NOT NULL,
                PRIMARY KEY (id)        
        );
        DROP TABLE IF EXISTS accounts CASCADE;
        CREATE TABLE accounts(
                id UUID NOT NULL,
                password VARCHAR(30) NOT NULL,
                PRIMARY KEY (id),
                CONSTRAINT fk_id FOREIGN KEY (id) REFERENCES users (id) ON DELETE CASCADE         
        );
    """
    )

    cursor.execute(
        """
            DROP TABLE IF EXISTS posts CASCADE;
        """
    )

    cursor.execute(
        """CREATE TABLE posts(
        sid SERIAL,
        id UUID NOT NULL,
        post TEXT NOT NULL,
        header VARCHAR(30),
        send TIMESTAMP,
        PRIMARY KEY (sid),
        CONSTRAINT fk_id FOREIGN KEY (id) REFERENCES users (id) ON DELETE CASCADE
    );
    ALTER TABLE posts
        ADD COLUMN document_with_idx tsvector;
    UPDATE posts
        SET document_with_idx = to_tsvector(header || ' ' || post);
    CREATE INDEX document_idx
        ON posts
        USING GIN (document_with_idx);      
    """
    )

    con.commit()
except (Exception, psycopg2.Error) as error:
    print("Error while initializing database", error)
finally:
    # closing database connection.
    if con:
        cursor.close()
        con.close()
        print("PostgreSQL connection is closed for user postgres")
