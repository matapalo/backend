#! /usr/bin/python3
import psycopg2
from psycopg2 import Error

con = None
try:
    con = psycopg2.connect(
        user="postgres", password="postgres", host="127.0.0.1", port="5432"
    )

    cursor = con.cursor()

    cursor.execute("""CREATE ROLE testuser WITH LOGIN PASSWORD 'password';""")
    con.commit()
    # Print PostgreSQL version
    # cursor.execute("SELECT version();")
    # record = cursor.fetchone()
    # print("You are connected to - ", record, "\n")
except (Exception, psycopg2.Error) as error:
    if con:
        con.rollback()

    print("Error while executing query to PostgreSQL", error)
finally:
    # closing database connection.
    if con:
        cursor.close()
        con.close()
        print("PostgreSQL connection is closed for user postgres")
