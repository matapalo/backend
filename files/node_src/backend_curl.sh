#! /bin/bash

server=192.168.55.55/
getCURL='curl -X GET $url'
postCURL='curl -X POST $url'

usage(){
    clear
    echo \
    "
    POST /posts
    $ post -p <id> <message>

    POST /login
    $ post -l <username> <password>

-----------------------------------------

    GET /       
    $ get -i

    GET /users
    $ get -u

    GET /posts
    $ get -p <user_id>
    "
}

post(){
    case $1 in
        (-p)
            end="posts --data id=$2 --data post=$3"
    ;;
        (-l)
            end="login --data username=$2 --data password=$3"
    ;;
        (*)
            end=""
            echo "Do nothing"
            exit 1
    ;;
    esac
    if [[ $end != "" ]]; 
        then
            url=$server$end
            echo $url
            eval $postCURL
    fi

}

get(){
    case $1 in
        (-i)
            end="index"
        ;;
        (-p)
            end="posts/$2"
        ;;
        (-u)
            end="users"
        ;;
        (*)
            end=""
            echo "Error: "
            exit 1
        ;;
    esac
    if [[ $end != "" ]]; 
        then
            url=$server$end
            echo $url
            eval $getCURL
    fi
}
