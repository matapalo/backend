const cors = require("cors");
const express = require("express");
const bodyParser = require("body-parser");
// Routes
const posts = require("./controllers/posts");
const register = require("./controllers/register");
const login = require("./controllers/login");
// for database connection, logging and configure node
const pool = require("./db/conn");
const logger = require("./utils/logger");
const config = require("./utils/config");

logger.info("connecting to postgredb:", config.POSTGRE_URI);

// yhteyden testaus
(async function() {
  const client = await pool
    .connect()
    .then(() => {
      logger.info("connected to postgredb");
    })
    .catch(error => {
      logger.error("error connection to postgredb", error.message);
    });

  client.release();
});

const app = express();

app.use(cors());

app.options("*", cors());

// body JSON-encoded
app.use(bodyParser.json());
// body URL-encoded
app.use(bodyParser.urlencoded({ extended: true }));

// application/json
app.use(express.json());
// application/x-www-form-urlencoded
app.use(express.urlencoded({ extended: true }));

app.use(express.static("build"));

app.use("/posts", posts);
app.use("/register", register);
app.use("/login", login);

app.get("/index", (req, res) => {
  res.status(200).send({ message: "hello from server" });
});

// testaukseen
app.get("/users", (req, res) => {
  const queryText = "SELECT * FROM users";
  pool
    .query(queryText)
    .then(result => res.status(200).json({ users: result.rows }))
    .catch(err => logger.error("database error:", err.message));
});

// yksikkö- ja integraatiotesteihin
module.exports = app;
