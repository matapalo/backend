const router = require("express").Router();
const pool = require("../db/conn");
const uuidv4 = require("uuid/v4");
const logger = require("../utils/logger");

// POST /register
router.post("/", (req, res) => {
  logger.info("BODY: ", req.body);
  const { username, password } = req.body.user;
  const uuid = uuidv4();

  (async () => {
    const client = await pool.connect();
    try {
      await client.query("BEGIN");
      const queryText =
        "INSERT INTO users(id, username) VALUES($1, $2) RETURNING id";
      const result = await client.query(queryText, [uuid, username]);
      const insertText = "INSERT INTO accounts(id, password) VALUES ($1, $2)";
      const insertAccountValues = [result.rows[0].id, password];
      const queryResult = await client.query(insertText, insertAccountValues);
      logger.info("register - result: ", queryResult);
      await client.query("COMMIT");
      res.status(201).send({ message: "Account created" });
    } catch (e) {
      await client.query("ROLLBACK");
      throw e;
    } finally {
      client.release();
    }
  })().catch(e => logger.error("Error in registering a user: ", e.stack));
});

module.exports = router;
