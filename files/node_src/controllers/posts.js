const router = require("express").Router();
const pool = require("../db/conn");
const logger = require("../utils/logger");

router.post("/", (req, res) => {
  const query = "INSERT INTO posts(id, post) VALUES($1, $2)";
  const { id, post } = req.body;

  logger.info("user id: ", id);
  logger.info("post: ", post);

  //const time = new Date().getTime();

  try {
    pool
      .query(query, [id, post])
      .then(results => {
        logger.info("insert into post - result: ", results);
        res.status(201).send("Created");
      })
      .catch(err => logger.error("Error executing query", err.stack));
  } catch (err) {
    logger.error("Error in query", err.message);
  }
});

router.get("/:id", async (req, res) => {
  const id = req.params.id;
  const query = "SELECT * FROM posts WHERE id = $1";
  const client = await pool.connect();
  try {
    const result = await client.query(query, [id]);
    logger.info("get posts by a user - result: ", result);
    let arr = [];
    arr = result.rows.map(item => {
      return item.post;
    });
    res.status(200).send(`post for user with id ${id}: ${arr.join("; ")}`);
  } catch (e) {
    logger.info("Error in get posts: ", e.message);
  } finally {
    client.release(true);
  }
});

module.exports = router;
