const router = require("express").Router();
const pool = require("../db/conn");
const {
  validate,
  checkValidationResults
} = require("../utils/express-validator");
const logger = require("../utils/logger");

// POST /login
router.post(
  "/",
  validate["password"](),
  (req, res, next) => checkValidationResults(req, res, next),
  (req, res) => {
    logger.info("BODY: ", req.body);
    const { username, password } = req.body.user;
    const querytext =
      "(SELECT a.id FROM users AS u INNER JOIN accounts AS a ON a.id = u.id \
    WHERE u.username = $1 AND a.password = $2)";

    pool
      .query(querytext, [username, password])
      .then(result => {
        logger.info("login - result: ", result.rows);
        result.rows.length
          ? res
              .status(200)
              .json({ id: result.rows[0].id, message: "Valid credentials" })
          : res.status(401).send({ message: "Unauthorized" });
      })
      .catch(err => console.error(err.message));
  }
);

module.exports = router;
