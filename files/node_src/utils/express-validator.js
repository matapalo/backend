const { check, validationResult } = require("express-validator");

const validate = {
  password: () => {
    return [
      check("user.password")
        .exists()
        .withMessage("Password does not exist")
        .isLength({ min: 5 })
        .withMessage("Password must be at least 5 characters in length.")
    ];
  }
};

var checkValidationResults = (req, res, next) => {
  const errors = validationResult(req);

  if (!errors.isEmpty()) {
    // statuscode 422 --> Unprocessable Entity
    return res.status(422).json({ errors: errors.array() });
  } else {
    next();
  }
};

module.exports = { validate, checkValidationResults };
