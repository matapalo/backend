let PORT = process.env.PORT;
let POSTGRE_URI = process.env.DB_POSTGRE_URI;

module.exports = {
  PORT,
  POSTGRE_URI
};
